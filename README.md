- [Installation](#sec-1)
- [Usage](#sec-2)


# Installation<a id="orgheadline1"></a>

-   On mac: Place at `` `~/Library/texmf/tex/NMBU:CIGENE` `` for Latex to be able to find the theme
-   On Windows: No idea

Good resuorce: <https://tex.stackexchange.com/questions/2897/where-to-place-custom-beamer-themes>

# Usage<a id="orgheadline2"></a>

State: 

```sh
% Theme settings
\usetheme{Cigene2}
\usecolortheme{Cigene2}
```
